# Bombs

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Status](#status)


## General info
A game in the Mario style, in which the player's task is to avoid falling bombs and opponents on the ground.


## Technologies
* C#


## Status
Project is:  _finished_


